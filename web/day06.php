<?php
    session_start();
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $gender = ['0' => "Nam", '1' => "Nữ"];
    $spe = ['MAT' => "Khoa học máy tính", 'KDL' => "Khoa học vật liệu"];
    $full_name = $gender_user = $department = $date = $address = $fileupload = '';
    if(!empty($_POST)) {
        if(isset($_POST['fullname']) && isset($_POST['gender']) && isset($_POST['department']) && isset($_POST['date']) &&  isset($_POST['address']) ) {
            $full_name = $_POST['fullname'];
            $department = $_POST['department'];
            $date = $_POST['date'];
            $address = $_POST['address'];
            $gender_user = $_POST['gender'];
        }
        $fileupload = basename($_FILES['fileupload']['name']);   
        // echo $fileupload;
        $flag = true;
        // lấy định dạng ảnh
        $imageFileType = pathinfo($fileupload,PATHINFO_EXTENSION);
        // kiểm tra định dạng ảnh
        if($imageFileType != '') {
            $allowtypes    = array('jpg', 'png', 'jpeg', 'gif');
            if(!in_array($imageFileType, $allowtypes)) {
                $flag = false;
            }
            
        }
        //get name image
        $nameImage = pathinfo($fileupload,PATHINFO_FILENAME);
        // name file upload
        $nameFileUpload = $nameImage.'_'.date('YmdHis').'.'.$imageFileType;

    
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .infor {
        display: flex;
    }

    .label_form {
        max-width: 100px;
    }

    .form {
        line-height: 30px;
        margin-top: 8px;
    }

    .label_form {
        background-color: #499ede;
        margin-right: 20px;
        color: #fff;
        border: 2px solid #0b67ad;
    }

    .icon_red {
        color: red;
    }

    .notification {
        display: flex;
        justify-content: start;
        margin: 0;
        color: red;
        line-height: 20px;
        margin-left: 10px;
    }

    .infor_text {
        line-height: 34px;
        margin: 0;
    }
    </style>
</head>

<body>
    <center>
        <div style="width:40% ;" class="web">
            <?php 
            $err =[];
             if(!empty($_POST)) {
                $fullname = $_POST['fullname'];
                $department = $_POST['department'];
                $date = $_POST['date'];

                if(empty($fullname)) {
                   $err['fullname'] = 'Hãy nhập tên';
                } 
                if(!isset($_POST['gender'])) {
                   $err['gender'] = 'Hãy nhập giới tính';
                }   
                if(empty($department)) {
                    $err['department'] = 'Hãy nhập khoa';
                 } 
                
                if(isset($_POST['date'])) {
                    if($_POST['date'] == '') {
                        $err['date'] = 'Hãy nhập ngày sinh';
                    } else {
                        list($dd,$mm,$yyyy) = explode('/',$_POST['date']);
                        if (!checkdate($mm,$dd,$yyyy)) {
                            $err['date'] = 'Hãy nhập ngày sinh đúng định dạng';
                        } 
                    }
                }
                if($flag == false) {
                    $err['uploadFile'] = 'File upload không hợp lệ';
                }
                
                if(sizeof($err) == 0) {
                    
                    if (!file_exists('uploads')) {
                        mkdir('uploads', 0777, true);
                    }
                    $target_dir    = "uploads/";
                    $target_file   = $target_dir.$nameFileUpload;
                    // CHÚ Ý: PHẦN NÀY CẦN THAY ĐỔI LINK THƯ MỤC "UPLOADS" NƠI SẼ CHỨA ẢNH TẠI MÁY CỦA BẠN
                    move_uploaded_file($_FILES["fileupload"]["tmp_name"],'F:/Góc tự học 2/Thực tập Web/W07/day07/web/uploads/'.$nameFileUpload);   

                    header("Location: confirm.php");
                    $_SESSION['fullname_session'] = $full_name;
                    $_SESSION['gender_session'] = $gender_user;
                    $_SESSION['department_session'] = $department;
                    $_SESSION['date_session'] = $date;
                    $_SESSION['address_session'] = $address;
                    $_SESSION['fileupload_session'] = $fileupload;
                    
                } 
                echo (isset($err['fullname']) ? '<p class="notification">'.$err['fullname'].'</p>' : '' );
                echo (isset($err['gender']) ? '<p class="notification">'.$err['gender'].'</p>' : '' );
                echo (isset($err['department']) ? '<p class="notification">'.$err['department'].'</p>' : '' );
                echo (isset($err['date']) ? '<p class="notification">'.$err['date'].'</p>' : '' );
                echo (isset($err['uploadFile']) ? '<p class="notification">'.$err['uploadFile'].'</p>' : '');
             }  
            ?>
            <form method="post" action="" enctype="multipart/form-data">
                <div class="infor form">
                    <label class="label_form" style="flex: 1">Họ và tên <span class="icon_red">*</span></label>
                    <input class="input" style="flex: 2; border: 2px solid #0b67ad;" name="fullname">
                </div>
                <div class="form" style=" display: flex;">
                    <label class="label_form" style="flex: 1">Giới tính <span class="icon_red">*</span></label>
                    <div>
                        <?php
                            for($i=0; $i< sizeof($gender); $i++ ) {
                                echo '<input class="input" style="background: #20b835" type="radio" value="'.$gender[$i].'"  name ="gender" '.'<label
                                style="margin-left: 6px">'.$gender[$i].'</label>';
                            }
                        ?>
                    </div>
                </div>
                <div class="form" style="display: flex;">
                    <label style="flex:1 ;" class="label_form">Phân khoa <span class="icon_red">*</span></label>
                    <?php
                            echo'<select class="input" name="department" style="border: 2px solid #0b67ad">
                            <option selected></option>."';
                            foreach($spe as $key => $value) {
                                echo '<option>'.$value.'</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="form" style=" display: flex;">
                    <label class="label_form" style="flex: 1">Ngày sinh <span class="icon_red">*</span></label>
                    <input class="datepicker input" name="date" placeholder="dd/mm/yyyy"
                        style="border: 2px solid #0b67ad; width: 137px">

                </div>
                <div class="infor form" style="display: flex;">
                    <label class="label_form label_form" style="flex: 1">Địa chỉ</label>
                    <input class="input" style="flex: 2; border: 2px solid #0b67ad; " name="address">
                </div>
                <div class="infor form" style="display: flex;">
                    <label class="label_form label_form" style="flex: 1; max-height: 34px;">Hình ảnh</label>
                    <input class="input" type="file" style="line-height: 34px;" name="fileupload">
                </div>

                <button class="btn_submit" onclick="hiddenInput()"
                    style="margin-top: 20px;height: 44px; width: 120px; border-radius: 6px; border: 2px solid #0b67ad; background-color: #20b835; color: #fff; font-size: 15px;">Đăng
                    ký</button>

            </form>
        </div>
    </center>

</body>


</html>